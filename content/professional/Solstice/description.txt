EINZIGARTIGES DESIGN, HANDGEMACHTE QUALITÄT
Das Arbor Solstice Axis 37" B4Bc wurde für smoothe Downhill- und Carving Sessions entwickelt. Das Board bietet ein angenehmes, stabiles Fahrgefühl, auch bei höherer Geschwindigkeit.

Ein Teil aller Verkäufe der Solstice Kollektion kommt dem Boarding For Breast Cancer zugute.

Riding Style
Ein vielseitiges Freeride Board für kreative Skater. Egal ob beim gemütlichen Cruisen und Carven in der Stadt, oder wenn du mit höherer Geschwindigkeit deine ersten Erfahrungen im Downhill-Bereich sammelst.

Shape
Die symmetrische Form ist perfekt zum Skaten in beide Richtungen, egal ob Forward oder Switch. Bei der Drop-Through Achsmontage liegt das Board um eine Deckbreite tiefer als beim Top Mount. Durch den niedrigen Schwerpunkt erhöht sich die Stabilität und Laufruhe.

Setup
Alle Arbor Skate Decks sind handgemacht und werden aus nachhaltigen Holzsorten gebaut. Die breiten Achsen in Reverse-Kingpin Bauweise sind wendig und sehr stabil. Die weichen Rollen bieten einen sehr guten Grip und rollen smooth über jeden Untergrund. Das Deck besteht aus 7 Schichten Ahornholz und einer Lage Palisander.


 

Hinweis: Die Farbe der Rollen und Achsen kann aufgrund von Lager-Engpässen variieren.

EIGENSCHAFTEN
Eigenschaften
Low Concave für Stabilität
Artikelnr.:
640533
Für:
Herren
Damen
Jungen
Mädchen
Materialangabe:
Drop Through
Öko:
Mit recycelten Materialien
Nachhaltig
Riding Style:
Freeriding
Boardbreite:
8.5 inch
Länge (inch):
37.0 inch
Breite:
21.59 cm
Länge (cm):
93.98 cm
Achsmontage:
Drop-Through
Achsstand:
27.75 inch
Achsstiftbreite:
9.6 inch
Hängerbreite:
7.1 inch
Rollen Ø:
69.0 mm
Härtegrad der Rollen:
78A
Lauffläche:
44.0 mm
ABEC-Wert:
7.0
Features
B4BC - Boarding For Breast Cancer
7-Schichten kanadisches Hardrock-Ahorn, eine Schicht Palisander
Holzmaterial stammt aus nachhaltigen Bezugsquellen
Holznebenprodukte werden für die Verwendung in anderen Produkten zurückgewonnen
Paris 50° 180 mm Reverse Kingpin Achsen
69 mm, 78a Arbor Wheels
ABEC 7 Kugellager
Grip Tape aus recyceltem Glas
