
GLOBE THE ALL TIME 35.875" (90,5CM) KOMPLETT-LONGBOARD (EXCESS)

![](https://gitlab.com/openbsd98324/skating/-/blob/main/content/longboard/35875905-excess-123887-0-Globe-TheAllTime35875905cm.jpg)





schrauben
===============
schrauben: 14mm+15mm = 29mm
type #1: 30mm 
type #2: 35mm ( so the plastic 5.5mm extension of height is required )



rollen/wheels 
===============
type #1: Globe Conical Cruiser 65 mm 78A Rollen (ok)
type #2: 4president  70mm (OK)
type #3: 75mm onrange inheat (back wheel)

delta: 5mm <--- 







145,99 EUR

Länge: 91,4 cm
Breite: 22,8 cm
Dicke: 1,4 cm
Wheelbase: 56,5 cm
 
Cruising / Carving
Resin-8 Hardrock Maple Konstruktion
acht Lagen kanadisches Hardrock Ahornholz mit Epoxidharz
leichter und langlebiger als Decks mit Kleber auf Wasser-Basis
Kicktail, Wheelwells
Low Concave
Stiff Flex
Topmount Achsmontage
 
Slant 150 mm Achsen / 150 mm Hangerbreite / 212 mm Achsstiftbreite
Globe Conical Cruiser 65 mm 78A Rollen
Globe ABEC 7 Kugellager
Kreuz-Montagesatz, 1/8" Riserpads
Griptape
